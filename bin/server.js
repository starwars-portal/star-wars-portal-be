process.env.PORT = process.env.PORT || 8080;
const port = process.env.PORT;
const ENABLE_HTTPS = process.env.ENABLE_HTTPS === 1;
process.env.NODE_ENV = process.env.NODE_ENV || 'local';
const env = process.env.NODE_ENV;

const http = require('http');
const app = require('../app.js')();
const DataManager = require('../services/DatabaseManager');

// ///// Create Database and tables
// DataManager.createDatabase((error, ok) => {
//   // Create Subscriber Table
//   if (!error) {

    DataManager.createSubscriberTable((error2) => {
      if (!error2) {
        console.log('Created Subscriber Table successfully!');
      }
      else {
        console.log(error2);
      }
    });
//   }
//   else {
//     console.log(error);
//   }
// });

const server = http.createServer(app);
server.listen(port);

server.on('error', (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const addr = server.address() || { port };
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
  switch (error.code) {
    case 'EACCES':
      console.log(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.log(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
});

server.on('listening', () => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
  console.log(`[${env}] ${ENABLE_HTTPS ? 'HTTPS' : 'HTTP'} Server is listening on ${bind}`);
});
