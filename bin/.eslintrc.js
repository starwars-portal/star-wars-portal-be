module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 8,
  },
  rules: {
    "linebreak-style": "off",
    "no-unused-vars": "warn",
    camelcase: "warn",
    "no-param-reassign": "warn",
    "max-len": "off",
    indent: "off",
    "no-underscore-dangle": "warn"

  },
};
