# Star War

Backend API

# Getting Started
#### Prerequisites
1. NodeJS https://nodejs.org/en/download/
2. Docker https://www.docker.com/

#### Starting the App
1. Run `npm install` in the directory terminal.
2. Run `npm start` in the directory terminal.
3. Access http://localhost:8080/ on your browser.

#### Deployment

To ensure that your application runs smoothly when deployed to our staging and production servers, please ensure that you are able to run your application via Docker.

1. Build the application:
```bash
docker build -t star-war-portal-be .
```
2. Run your application:
```bash
docker run -p 3000:3000 star-war-portal-be
```

If you need to stop to restart your container, you may do so through the following commands
1. List all running containers:
```bash
docker ps
2. Stop your container:
```bash
docker stop <container id>
```
3. Delete the container:
```bash
docker rm <container id>
```