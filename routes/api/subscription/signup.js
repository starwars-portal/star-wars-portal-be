const { Router } = require('express');
const Subscription = require('../../../models/Subscription');
const recaptchaV3Validator = require('./../../../middlewares/recaptchaV3Validator');
const s3 = require('../../../services/s3');
const multipart = require('connect-multiparty')();

module.exports = Router()
  .post('/subscription', multipart, recaptchaV3Validator, async (req, res) => {

    try {
      const params = req.body;
      if (req.files !== null && req.files !== undefined && req.files.file !== null && req.files.file !== undefined) {
        const uploadFile = req.files.file;
        console.log(`uploading file ${uploadFile.originalFilename} `);
        const buffer = await s3.readUploadedFile(uploadFile.path);
        if (buffer !== null) {
            const dataUploaded = await s3.upload(uploadFile, buffer);
            params.profile_image = dataUploaded.Location;
            console.info(`uploaded to s3: ${params.profile_image} `);
        }
      }
      // console.log('agreed terms, condition, privacy policy');
      Subscription.signup(params, (error, result) => {
        if (result) {
          res.status(200).json({
            message: 'OK',
            id: result,
          });
        } else {
          res.status(400).json({
            error: error.toString(),
            code: 400,
          });
          ;
        }
      });
    } catch (error) {
      res.status(400).json({
        error: error.toString(),
        code: 400,
      });
    }
  });
