const { Router } = require('express');
const Subscription = require('../../../models/Subscription');

module.exports = Router({ mergeParams: true })
  .get('/subscription/list', (req, res) => {
    try {
      const params = {
        offset: req.query.offset || 0,
        limit: req.query.limit,
      };

      Subscription.getListSubscribers(params, (error, result) => {
        if (result) {
          res.status(200).json(result);
        } else {
          res.status(400).json({
            error: error.toString(),
            code: 400,
          });
        }
      });
    } catch (error) {
      res.status(400).json({
        error: error.toString(),
        code: 400,
      });
    }
  });
