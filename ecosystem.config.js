module.exports = {
  apps: [{
    name: 'API',
    script: 'bin/server.js',
    exec_mode: 'cluster',

    // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
    instances: 4,
    autorestart: true,
    watch: false,
  }],
};
