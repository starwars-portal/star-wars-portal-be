'use strict';

const dbConfig = require('../config/configFactory').get('main_db.config');
const QueryBuilder = require('node-querybuilder');

module.exports = class BaseModel {
    constructor() {
        this.queryBuilder = new QueryBuilder(dbConfig, 'mysql', 'pool');
    }

    getTable() {
        throw new Error('Not found table');
    }

    /*
    * filter valid fields
    */
    async filterFields(fields) {

        if (fields === '*') return fields;

        if (!Array.isArray(fields)) {
            fields = fields.split(',');
        }

        let conn = await this.queryBuilder.get_connection();
        let data = await conn.query('SHOW COLUMNS FROM ' + this.getTable());

        let columns = [];
        Object.keys(data).forEach((key) => {
            columns.push(data[key].Field)
        });

        let validFields = fields.filter((field) => {
            return columns.includes(field);
        });

        return validFields;
    }
    
    /*
     * execute query
     */
    async find(filters, fields, limit = null, offset = 0) {
        let conn = await this.queryBuilder.get_connection();
        let table = this.getTable();

        fields = await this.filterFields(fields);

        let result;

        if (Object.keys(filters).length > 0) {
            if (limit != null && !isNaN(limit)) {
                result = await conn.select(fields)
                    .where(filters)
                    .limit(limit, offset)
                    .get(table);
            }
            else {
                result = await conn.select(fields)
                    .where(filters)
                    .get(table);
            }
        }
        else {
            if (limit != null && !isNaN(limit)) {
                result = await conn.select(fields)
                    .limit(limit, offset)
                    .get(table);
            }
            else {
                result = await conn.select(fields).get(table);
            }
        }
        conn.release();

        console.log('last_query:', conn.last_query());
        console.log('result:', result);

        return result;
    }

    async findAll(filters, fields, limit = null, offset = 0) {
        return this.find(filters, fields, limit = null, offset = 0);
    }

    async findRow(filters, fields) {
        let data = await this.find(filters, fields);
        if (Array.isArray(data) && data.length > 0) {
            return data[0];
        }
    }
}