const xss = require('xss');
const db = require('../services/db');
const SubscriberModel = require('./Subscriber.js');

module.exports = class Subscription {
    static validate(params) {
        const result = { error: null };

        if (params.email === null || params.email === undefined || params.email === '') {
            result.error = 'email is empty';
        } else if (!params.email.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)) {
            result.error = 'email is invalid';
        }
        return result;
    }

    static signup(params, callback) {
        // validate data
        const validation = this.validate(params);
        if (validation.error !== null) {
            callback(validation.error, null);
            return;
        }

        const subscriber = new SubscriberModel();
        // sanitize data
        for (const prop in subscriber) {
            subscriber[prop] = xss(params[prop]);
        }

        // check if email existed already

        let sql = 'SELECT `id` FROM `subscriber` WHERE `email` = ?';
        db.query(sql, subscriber.email, (err, result) => {
            if (result && Array.isArray(result) && result.length > 0) {
                // update data if the email exists
                const { id } = result[0];
                sql = 'UPDATE `subscriber` SET ? WHERE id = ?';
                db.query(sql, [subscriber, id],
                    (queryErr, updateRes) => {
                        if (queryErr) {
                            console.log('Signup error: ', queryErr);
                            callback(queryErr, null);
                        } else {
                            // console.log(result);
                            callback(null, id);
                        }
                    });
            } else {
                // insert new record
                sql = 'INSERT INTO `subscriber`(`email`, `firstname`, `lastname`, `profile_image`) VALUES (?)';
                db.query(sql, [Object.values(subscriber)],
                    (queryErr, insertRes) => {
                        if (queryErr) {
                            console.log('Signup error: ', queryErr);
                            callback(queryErr, null);
                        } else {
                            // console.log(result);
                            callback(null, insertRes.insertId);
                        }
                    });
            }
        });
    }

    static getListSubscribers(params, callback) {
        let { limit, offset } = params;

        let sql = 'SELECT `id`, `email`, `firstname`, `lastname`, `profile_image`, `date_created`, `date_modified` FROM `subscriber`';
        sql += ' ORDER BY date_created DESC';

        if (limit && parseInt(limit) > 0) {
            offset = (offset && parseInt(offset) > 0) ? offset : 0;
            sql += ` LIMIT ${offset}, ${limit}`;
        }

        db.query(sql, (queryErr, result, fields) => {
            if (queryErr) {
                console.log('Query error: ', queryErr);
                callback(queryErr, null);
            } else {
                // console.log(result);
                callback(null, result);
            }
        });
    }
};
