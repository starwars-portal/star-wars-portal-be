const axios = require('axios');
const qs = require('querystring');
const VERIFY_URL = global.gConfig.recaptcha_v3_api_site_verify || 'https://www.google.com/recaptcha/api/siteverify';
const SECRET_KEY = global.gConfig.recaptcha_v3_secrect_key || '6LcQJtgUAAAAANYPaZte1uoVbr4JAMV4TacbV-XT';

module.exports = async (req, res, next) => {
    try {
        // by pass recaptcha if local or staging environment
        // if (process.env.NODE_ENV === 'local' || process.env.NODE_ENV === 'staging') {
        //     return next();
        // }

        console.log("req.body.captcha:", req.body.captcha)

        const token = req.query.captcha || req.body.captcha || '';

        const options = {
            method: 'POST',
            url: VERIFY_URL,
            data: qs.stringify({
                secret: SECRET_KEY,
                response: token
            })
        };

        const {data} = await axios(options);

        if(data) {
            if(data.success) {
                if(data.score >= 0.3)
                    return next();
                else {
                    return res.status(400).json({
                        error: "recaptcha bot detected",
                        code: 400
                    });
                }
            }

            return res.status(400).json({
                error: data,
                code: 400
            });
        }

        return res.status(400).json({
            error: 'request verify captcha fail',
            code: 400
        });
    }
    catch (error) {
        return res.status(400).json({
            error: error.message,
            code: 400
        });
    }
}