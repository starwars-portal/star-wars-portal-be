// global
global.root_dir = __dirname;
const cors = require('cors');
const _ = require('lodash');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
const cookieParser = require('cookie-parser');

const config = require('./configs/config');

const defaultConfig = config.local;
const environment = process.env.NODE_ENV || 'local';
const environmentConfig = config[environment];
const finalConfig = _.merge(defaultConfig, environmentConfig);
global.gConfig = finalConfig;
global.gConfig.database.host = process.env.RDS_HOSTNAME || global.gConfig.database.host;
global.gConfig.database.port = process.env.RDS_PORT || global.gConfig.database.port;
global.gConfig.database.database = process.env.RDS_DB_NAME || global.gConfig.database.database;
global.gConfig.database.user = process.env.RDS_USERNAME || global.gConfig.database.user;
global.gConfig.database.password = process.env.RDS_PASSWORD || global.gConfig.database.password;

console.log(`NODE_ENV: ${environment}`);

const app = express();
app.use(cors());
app.options('*', cors());

// cookie setup
app.use(cookieParser());
// session setup

const options = {
    // Host name for database connection:
    host: global.gConfig.database.host,
    // Port number for database connection:
    port: global.gConfig.database.port,
    // Database user:
    user: global.gConfig.database.user,
    // Password for the above database user:
    password: global.gConfig.database.password,
    // Database name:
    database: global.gConfig.database.database,
    // Whether or not to automatically check for and clear expired sessions:
    clearExpired: true,
    // How frequently expired sessions will be cleared; milliseconds:
    checkExpirationInterval: 900000,
    // The maximum age of a valid session; milliseconds:
    expiration: 86400000,
    // Whether or not to create the sessions database table, if one does not already exist:
    createDatabaseTable: true,
    // Number of connections when creating a connection pool:
    connectionLimit: 1,
    // Whether or not to end the database connection when the store is closed.
    // The default value of this option depends on whether or not a connection was passed to the constructor.
    // If a connection object is passed to the constructor, the default value for this option is false.
    endConnectionOnClose: true,
    charset: global.gConfig.database.charset,
    schema: {
        tableName: 'sessions',
        columnNames: {
            session_id: 'session_id',
            expires: 'expires',
            data: 'data',
        },
    },
};
const sessionStore = new MySQLStore(options);
app.set('trust proxy', 1); // trust first proxy
const sess = {
    secret: `start-wars-portal-be-${environment}`,
    resave: false,
    saveUninitialized: false,
    store: sessionStore,
    cookie: {
        maxAge: 3600000 * 24 * 7 * 52,
        secure: 'auto', // serve secure cookies
    },
};
app.use(session(sess));

// body parser setup
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('Hi!');
});

module.exports = () => app
    .use(logger('dev'))
    .use((req, res, next) => {
        req.base = `${req.protocol}://${req.get('host')}`;
        return next();
    })
    .use('/api', require('./routes/api/createRouter.js')());
