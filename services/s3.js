const AWS  = require ('aws-sdk');
const fs = require ('fs');

const ID = process.env.AWS_S3_ACCESS_KEY_ID;
const SECRET = process.env.AWS_S3_SECRET_ACCESS_KEY;


AWS.config.update({region: 'ap-southeast-1'});

let s3;
let bucket;
if (process.env.NODE_ENV !== 'local') {
    s3 = new AWS.S3({
        apiVersion: '2006-03-01',
        accessKeyId: ID,
        secretAccessKey: SECRET
    });
    bucket = 'star-wars/' + process.env.NODE_ENV;
}
else {
    s3 = new AWS.S3({
        apiVersion: '2006-03-01'
    });
    bucket = 'star-wars/develop';
}

/**
 *
 * @param file
 * @param callback see https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#upload-property
 */

function upload(file, buffer) {
	return s3
		.upload({
			Bucket: bucket,
			Key: file.originalFilename,
			Body: buffer,
			ACL: 'public-read',
			CacheControl: 'public, max-age=86400',
		})
		.promise();
}

function readUploadedFile(path) {
    return new Promise((resolve) => {
        fs.readFile(path, (err, data) => {
            if (!err) {
                resolve(data)
            }
            else {
                logger.warn(`Failed to read ${path} : ${err}`);
                resolve(null);
            }
        });
    });
}

module.exports = {
    upload,
    readUploadedFile
};