const arraySort = require('array-sort');
const glob = require('glob');
const fs = require("fs");
const dataDir = `${global.root_dir}/data/`;
module.exports = class DataManager {
    static load(path, filename = '*') {
        let data;
        if (filename != '*') {
            // read a specific file
            try {
                const filepath = `${dataDir}${path}/${filename}.json`;
                data = JSON.parse(fs.readFileSync(filepath));
            }
            catch (e) {
                console.log('Error when loading data: ', e);
                return false;
            }
        }
        else {
            // read all json files in a folder
            try {
                const jsonFiles = glob.sync(`${dataDir}${path}/${filename}.json`, {cwd: `${__dirname}/`});
                data = [];
                jsonFiles.map(filepath => {
                    try {
                        let content = fs.readFileSync(filepath);
                        data.push(JSON.parse(content));
                    } catch (e) {
                        console.log('Parsing error file: ', filepath);
                    }

                });
            }
            catch (e) {
                console.log('Error when loading data: ', e);
                return false;
            }
        }

        return data;
    }

    static find(params) {
        const {folder, search, sort} = params;

        if (!folder) {
            return [];
        }

        let data = DataManager.load(folder);

        // filter data match search conditions
        if (search) {
            data = data.filter((item) => {
                let matched = false;
                let key;
                for(key in search) {
                    matched = (item.hasOwnProperty(key) && item[key] == search[key])
                }
                return matched;
            });
        }

        // sort data
        if (sort && sort.by) {
            data = arraySort(data, sort.by, { reverse: sort.reverse ? sort.reverse: false })
        }
        return data;
    }
}