const mysql = require('mysql');


// Create the global connection
const gConnection = mysql.createConnection(global.gConfig.database);

gConnection.connect(function(err) {
    if (err) {
      console.error(`Database connection failed to ${global.gConfig.database.database} host: ${global.gConfig.database.host}: ${err.stack}`);

      return;
    }
  
    console.log('Connected to database.');
  });
gConnection.end = function () {
    console.log('DO NOT END this connection!!!!!!!!!!!!!!!!!!!');
};

class MySqlWrapper {
    /**
     * https://www.npmjs.com/package/mysql2#first-query
     * @param query
     * @param params
     * @param callback
     */
    static query(query, params, callback) {
        gConnection.query(query, params, callback);
    }


    /**
     * DO NOT END this connection!!!!!!!!!!!!!!!!!!!
     *
     * @returns {Connection}
     */
    static getGlobalConnection() {
       return gConnection;
    }

    static promiseQuery = async ( query, params ) => { return new Promise( ( resolve, reject ) => {
        const handler = ( error, result ) => {
            if( error )
                return reject( error );
            resolve( result );
        };
        gConnection.query( query, params, handler );
    } ) };
}

module.exports = MySqlWrapper;
