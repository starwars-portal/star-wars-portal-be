const mysql = require('mysql');
const db = require('./db');

module.exports = class DatabaseManager {
    static createDatabase(callback) {
      const connection = mysql.createConnection({
            host: global.gConfig.database.host,
            user: global.gConfig.database.user,
            password: global.gConfig.database.password,
        });
        connection.query(`CREATE DATABASE IF NOT EXISTS ${global.gConfig.database.database}`, (error, results, fields) => {
            connection.end();
            callback(error, results);
        });
    }

    static createSubscriberTable(callback) {
      const sql = 'CREATE TABLE IF NOT EXISTS '
          + 'subscriber ( '
          + '  id int(11) AUTO_INCREMENT PRIMARY KEY, '
          + '  email varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, '
          + '  firstname varchar(255) CHARACTER SET utf8 NOT NULL, '
          + '  lastname varchar(255) CHARACTER SET utf8 NOT NULL, '
          + '  profile_image MEDIUMTEXT CHARACTER SET utf8 NOT NULL, '
          + '  date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, '
          + '  date_modified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, '
          + '  INDEX USING BTREE (email) '
          + ') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci';
      db.query(sql, (err, result) => {
        callback(err, result);
      });
    }
};
