FROM node:12.16.1-alpine3.9

WORKDIR /app
COPY . .

RUN apk update
RUN apk add git
RUN apk add bash
RUN apk add python

RUN npm install pm2@4.2.3 -g

RUN npm install
RUN npm audit fix

EXPOSE 3000
CMD ["pm2-runtime", "start", "ecosystem.config.js"]
# CMD ["node", "bin/server.js"]
